def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a + b >= c and b + c >= a and c + a >= b:
        return True
    else:

         return False

param_a = float(input('Length of the first side: '))
param_b = float(input('Length of the second side: '))
param_c = float(input('Length of the third side: '))

if is_triangle(param_a, param_b, param_c):
    print('True')
else:
    print('False')